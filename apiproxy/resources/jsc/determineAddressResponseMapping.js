var dConfirmed = context.getVariable("soapresponse.deliveryConfirmed");
var valid = context.getVariable("soapresponse.valid");
var couldNotValidate = context.getVariable("soapresponse.couldNotValidate");
var scode = context.getVariable("soapresponse.scode");
var response_line2 = context.getVariable("soapresponse.line2");
var response_additionalInfo = context.getVariable("soapresponse.additionalInfo");
var input_line1 = context.getVariable("line1");
var input_line2 = context.getVariable("line2");
var input_line3 = context.getVariable("line3");
var input_line4 = context.getVariable("line4");
var input_city = context.getVariable("city");
var input_state = context.getVariable("hashedState");
var input_country = context.getVariable("soapresponse.country");
var input_postalCode = context.getVariable("postalCode");
//adding additional address details.
var apartmentNumber = context.getVariable("soapresponse.apartmentNumber");
var houseNumber = context.getVariable("soapresponse.houseNumber");
var leadingDirectional = context.getVariable("soapresponse.leadingDirectional");
var streetName = context.getVariable("soapresponse.streetName");
var streetSuffix = context.getVariable("soapresponse.streetSuffix");
var trailingDirectional = context.getVariable("soapresponse.trailingDirectional");
var usCountyName = context.getVariable("soapresponse.usCountyName");


//fixing barcode value not to have null vlaues .
//{soapresponse.pbase}{soapresponse.padd}{soapresponse.pcode}{soapresponse.pusb}

var pbase = context.getVariable("soapresponse.pbase");
var padd = context.getVariable("soapresponse.padd");
var pcode = context.getVariable("soapresponse.pcode");
var pusb = context.getVariable("soapresponse.pusb");

if (pbase == 'NULL' || pbase === null)
{
     context.setVariable("soapresponse.pbase", '');
}

if (padd == 'NULL' || padd === null)
{
     context.setVariable("soapresponse.padd", '');
}

if (pcode == 'NULL' || pcode === null)
{
     context.setVariable("soapresponse.pcode", '');
}

if (pusb == 'NULL' || pusb === null)
{
     context.setVariable("soapresponse.pusb", '');
}





if (input_country !== null)
{
 input_country = input_country.substring(0, 13)   
}

if (apartmentNumber == 'NULL' || apartmentNumber === null)
{
     context.setVariable("soapresponse.apartmentNumber", '');
}

if (houseNumber == 'NULL' || houseNumber === null)
{
     context.setVariable("soapresponse.houseNumber", '');
}


if (leadingDirectional == 'NULL' || leadingDirectional === null)
{
     context.setVariable("soapresponse.leadingDirectional", '');
}


if (streetName == 'NULL' || streetName === null)
{
     context.setVariable("soapresponse.streetNAme", '');
}


if (streetSuffix == 'NULL' || streetSuffix === null)
{
     context.setVariable("soapresponse.streetSuffix", '');
}


if (trailingDirectional == 'NULL' || trailingDirectional === null)
{
     context.setVariable("soapresponse.trailingDirectional", '');
}

if (usCountyName == 'NULL' || usCountyName === null)
{
     context.setVariable("soapresponse.usCountyName", '');
}


if (scode !== null && couldNotValidate !== null) {
        context.setVariable("soapresponse.scode", scode + ":" + couldNotValidate);
}
if (valid === null) {
    //if no status indicator in SOAP response, then this is a valid address so use response values for JSON address response fields mapping.
    context.setVariable("soapresponse.valid", "Y");
    context.setVariable("soapresponse.country", input_country);
   
    if (response_line2 === null || response_line2 == 'NULL') {
        context.setVariable("soapresponse.line2","");
        if (response_additionalInfo === null || response_additionalInfo == 'NULL') {
           context.setVariable("soapresponse.line2","");
        } else {
           context.setVariable("soapresponse.line2", response_additionalInfo);
        }
    } else {
        context.setVariable("soapresponse.line2", response_line2 + ' ' + response_additionalInfo);
    }
} else {
    //if status indicator is present in soap response, then this is an invalid address so use input values for JSON response fields mapping.
	context.setVariable("soapresponse.line1", input_line1);
	context.setVariable("soapresponse.line2", input_line2);
	context.setVariable("soapresponse.line3", input_line3);
	context.setVariable("soapresponse.line4", input_line4);
	context.setVariable("soapresponse.city", input_city);
	context.setVariable("soapresponse.state", input_state);
	context.setVariable("soapresponse.postalCode", input_postalCode);
	context.setVariable("soapresponse.country", input_country);
} 
if (dConfirmed != 'Y') {
    context.setVariable("soapresponse.deliveryConfirmed", 'N');
}







