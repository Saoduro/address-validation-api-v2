//This code converts the query parameters to JSON to prepare them for schema validation
var jsonOutput = '';
var queryStringPairs = context.getVariable("request.querystring").split("&");
queryStringPairs.forEach(function(nameValuePair) {
	var nameAndValue = nameValuePair.split('=');
	jsonOutput += '"' + nameAndValue[0] + '"' + ':' + '"' + nameAndValue[1] + '"' + ',';
});
jsonOutput = '{' + jsonOutput.substr(0, jsonOutput.length -1) + '}';
context.setVariable('request.content', jsonOutput);